#include<bits/stdc++.h>
using namespace std;

int main(){

  int n ;               // Declare integer variable .  Size = 4 bits .
  scanf("%d" , &n ) ;   // assign value to variable n .
  printf("%d" , n );    // print this value .
  
  long long  n ;        // Declare long long  variable .  Size = 8 bits .
  scanf("%lld" , &n ) ; // assign value to variable n .
  printf("%lld" , n );  // print this value .
  
  float n ;             // Declare float variable .  Size = 4 bits .
  scanf("%f" , &n ) ;   // assign value to variable n .
  printf("%f" , n );    // print this value .
  
  double n ;            // Declare integer variable .  Size = 8 bits .
  scanf("%lf" , &n ) ;   // assign value to variable n .
  printf("%lf" , n );    // print this value .
  
  
  printf("%.3f" , n ) ;  // print 3 digits -> 1.005 .
  printf("%.3lf" , n ) ;  // print 3 digits -> 1.575 .
  printf("%.3d" , n ) ;  // print 3 digits -> 1.000 .
  printf("%.3lld" , n ) ; // print 3 digits -> 1.000 .
  
  
  char n ;               // Declare char variable . 
  scanf("%c" , &n ) ;   // assign value to variable n .
  printf("%c" , n );    // print this value .
  
  
  char n[12] ;          // Declare string variable .  lenght  = 12 char .
  scanf("%s" , &n ) ;   // assign value to string n .
  printf("%s" , n );    // print this value .
  
  
  char n[12] ;           // Declare string variable .  lenght  = 12 char .
  gets(n) ;              // assign value to string n . Note : all line .
  printf("%s" , n );    // print this value .
  
  return 0 ;

}